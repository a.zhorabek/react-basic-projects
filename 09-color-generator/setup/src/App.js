import React, { useState } from "react";
import SingleColor from "./SingleColor";

import Values from "values.js";

function App() {
  const [shades, setShades] = useState(10);
  const [color, setColor] = useState("");
  const [error, setError] = useState(false);
  const [list, setList] = useState(new Values("#a964a6").all(shades));

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      let colors = new Values(color).all(shades);
      setError(false);
      setList(colors);
    } catch (error) {
      setError(true);
      console.log(error);
    }
  };
  return (
    <>
      <section className="container">
        <h3>colour generator</h3>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            value={color}
            onChange={(e) => setColor(e.target.value)}
            name=""
            id=""
            placeholder="#f15025"
            className={`${error ? "error" : null}`}
          />
          <button className="btn" onClick={() => setShades(20)}>
            10
          </button>
          <button className="btn" onClick={() => setShades(10)}>
            20
          </button>
          <button className="btn" type="submit">
            submit
          </button>
        </form>
      </section>
      <section className="colors">
        {list.map((color, index) => {
          return (
            <SingleColor
              key={index}
              {...color}
              index={index}
              shades={shades}
              hexColor={color.hex}
            />
          );
        })}
      </section>
    </>
  );
}

export default App;
