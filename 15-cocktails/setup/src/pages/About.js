import React from "react";

const About = () => {
  return (
    <section className="section about-section">
      <h1 className="section-title">about us</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae sint
        nulla quaerat natus accusantium ducimus nisi? Dicta culpa odit illo,
        repellendus at, veniam cumque modi, laboriosam ipsum ut consectetur
        totam.
      </p>
    </section>
  );
};

export default About;
